import React, { Component } from 'react';
import Products from './Products';
import Filter from './Filter';
import ProductDetail from './ProductDetail';

import '../styles/Shop.css';
import { stat } from 'fs';

class Shop extends Component<{ handleCart, loggedInStatus }, any> {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            value: "",
            products: [],
            filteredProducts: [],
            productDetail: [],
            size: "",
            sort: "",
            dressType: '',
            minPrice: '',
            maxPrice: '',
            inCart: false,
            cartItems: []
        }; // A state component to store the result from database query
        this.toggle = this.toggle.bind(this);
        this.handleChangeDressType = this.handleChangeDressType.bind(this);
        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.handleChangeSize = this.handleChangeSize.bind(this);
        this.handleChangeMin = this.handleChangeMin.bind(this);
        this.handleChangeMax = this.handleChangeMax.bind(this);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.handleDetail = this.handleDetail.bind(this);
        this.filterByPrice = this.filterByPrice.bind(this);
    }

    toggle() {
        this.setState({
            show: !this.state.show
        });
    }
    callAPI() {
        // Using the backend server address for calling the API
        fetch("http://localhost:8000/nadassi")
            .then(res => res.json())
            .then(data =>
                // console.log(products);
                this.setState({
                    products: data,
                    filteredProducts: data
                }) // Stores the result to products component
            );
    }
    addProduct = (e) => {
        e.preventDefault();

        try {
            const data = {
                name: "Adalynn Long Dress",
                type: "Crafted drapes, V-shaped back, Bra cups"
            }
            fetch("http://localhost:8000/nadassi", {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
                .then(res => {
                    console.log(res);
                    return res.json()
                })
        } catch (error) {
            console.log(error);
        }
    }
    componentDidMount() {
        // fetching data from localstorage to retain when refreshed
        if (localStorage.getItem('cartItems')) {
            this.setState({ cartItems: JSON.parse(localStorage.getItem('cartItems')) });
        }
        this.callAPI();

    }
    listProducts = async () => {
        await this.setState(state => {
            if (state.sort !== '') {
                state.products.sort((a, b) =>
                    (state.sort === 'lowestprice'
                        ? ((a.price > b.price) ? 1 : -1)
                        : ((a.price < b.price) ? 1 : -1)));
            } else {
                state.products.sort((a, b) => (a.id > b.id) ? 1 : -1);
            }

            if (state.size !== '') {
                return { filteredProducts: state.products.filter(a => a.availableSizes.indexOf(state.size.toUpperCase()) >= 0) };
            }

            if (state.dressType !== '') {
                console.log(state.dressType);
                return { filteredProducts: state.products.filter(a => a.DressType === state.dressType) };
            }

            if (state.minPrice !== '' && state.maxPrice === '') {
                console.log(state.minPrice);

                return { filteredProducts: state.products.filter(a => a.price >= state.minPrice) }
            } else if (state.minPrice === '' && state.maxPrice !== '') {
                console.log(state.maxPrice);

                return { filteredProducts: state.products.filter(a => a.price <= state.maxPrice) }
            } else if (state.minPrice !== '' && state.maxPrice !== '') {
                return { filteredProducts: state.products.filter(a => state.minPrice <= a.price && a.price <= state.maxPrice) }
            }

            return { filteredProducts: state.products };
        })
    }
    getItem = id => {
        const product = this.state.products.find(item => item.id === id);
        return product;
    }
    handleDetail = (e, product) => {

        this.setState(() => {
            return {
                productDetail: product,
                show: !this.state.show
            }
        })
    }
    handleAddToCart = async (e, product, bodytype) => {
        const cartItems = this.state.cartItems;
        const addedItem = this.state.products.find(item => item._id === product._id);
        const price = addedItem.price;

        // const merge = (...object) => ({ ...object }); //taking two arguments and attaching together
        // const cartItem = merge(addedItem, bodytype, price);
        
        
        const cartItem = {addedItem, bodytype, price}
        console.log(cartItem);
        console.log(cartItems);
        let inCart = false;

        cartItems.forEach(cp => {
            if (cp.addedItem._id === cartItem.addedItem._id) {
                console.log("Product already in cart");
                inCart = true;
                cp.count += 1;
            }
        });

        if (!inCart) {
            cartItems.push({ ...cartItem, count: 1 });
            console.log("pushed");
        }
        localStorage.setItem('cartItems', JSON.stringify(cartItems));
        this.props.handleCart(cartItems);

        return { cartItems: cartItems };
    };

    handleChangeDressType = async (e) => {
        await this.setState({ dressType: e.target.value });
        this.listProducts();
    }

    handleChangeSort = (e) => {
        this.setState({ sort: e.target.value });
        this.listProducts();
    }
    handleChangeSize = (e) => {
        if (e.target.checked === true) {
            this.setState({ size: e.target.value });
        } else {
            this.setState({ size: "" });
        }

        this.listProducts();
    }

    handleChangeMin = async (e) => {
        await this.setState({ minPrice: e.target.value });
        // this.listProducts();
    }
    handleChangeMax = async (e) => {
        await this.setState({ maxPrice: e.target.value });
        // this.listProducts();
    }

    filterByPrice() {
        this.listProducts();
    }

    render() {
        return (
            <div className="wrapper">
                {this.state.show ?
                    <div className="wrapper-shop-detail">
                        <a className="button-close-detail" onClick={this.toggle}></a>
                        <ProductDetail
                            productDetail={this.state.productDetail}
                            handleAddToCart={this.handleAddToCart} />
                    </div> :
                    <div className="wrapper-shop">
                        <div className="filter">
                            <Filter
                                // size={this.state.size}
                                sort={this.state.sort}
                                dressType={this.state.dressType}
                                minPrice={this.state.minPrice}
                                maxPrice={this.state.maxPrice}
                                count={this.state.filteredProducts.length}
                                handleChangeDressType={this.handleChangeDressType}
                                handleChangeSize={this.handleChangeSize}
                                handleChangeSort={this.handleChangeSort}
                                handleChangeMin={this.handleChangeMin}
                                handleChangeMax={this.handleChangeMax}
                                filterByPrice={this.filterByPrice}
                            />
                        </div>
                        <div className="collection">
                            <h1>NEW COLLECTION 2019</h1>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and
                                typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a
                                type specimen book. It has survived not only five centuries,
                                but also the leap into electronic typesetting
            </p>
                        </div>
                        <div className="items">
                            <Products
                                products={this.state.filteredProducts}
                                sort={this.state.sort}
                                handleChangeSort={this.handleChangeSort}
                                handleDetail={this.handleDetail}
                                handleAddToCart={this.handleAddToCart} />
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default Shop