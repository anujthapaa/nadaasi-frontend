import React, { Component } from 'react';

import Support from '../images/contact/Icons/support.svg';
import Suggestion from '../images/contact/Icons/suggestion.svg';
import JoinUs from '../images/contact/Icons/join-us.svg';
import HomeIcon from '../images/contact/Icons/home.svg';
import CallIcon from '../images/contact/Icons/telephone.svg';
import MailIcon from '../images/contact/Icons/envelope.svg';

const validEmailRegex =
    RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

class Contact extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            subject: '',
            email: '',
            message: '',
            errors: {
                name: '',
                subject: '',
                email: '',
                message: ''
            }
        };

        this.handleSubmitFeedback = this.handleSubmitFeedback.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        event.preventDefault();

        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'name':
                errors.name =
                    value.length < 5
                        ? 'name must be at least 5 characters long'
                        : '';
                break;
            case 'subject':
                errors.subject =
                    value.length < 5
                        ? 'Subject must be at least 5 characters long'
                        : '';
                break;
            case 'email':
                errors.email =
                    validEmailRegex.test(value)
                        ? ''
                        : 'Email is not valid';
                break;
            case 'message':
                errors.message =
                    value.length < 8
                        ? 'Message must be at least 8 characters long'
                        : '';
                break;
            default:
                break;

        }

        this.setState({ errors, [name]: value }, () => {
            console.log(errors, value)
        })
    }

    handleSubmitFeedback = async (event) => {
        const { name, subject, email, message } = this.state

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                subject: subject,
                email: email,
                message: message
            })
        };

        await fetch("http://localhost:8000/feedback", options)
            .then(res => console.log(res))
            .catch(error => {
                console.log("feedback input error", error);
            })
    }

    render() {
        const { errors } = this.state;

        return (
            <div className="wrapper">
                <div className="wrapper-item contact-map">
                    <div className="contact-text">
                        <h1>Contact Us</h1>
                    </div>
                </div>
                <div className="wrapper-item contact-details">
                    <div className="company-info">
                        <div className="company-info-1">
                            <p>Company information</p>
                        </div>
                        <div className="company-info-2">
                            <img src={HomeIcon} alt="Address" />
                        </div>
                        <div className="company-info-3">
                            <p>
                                Suksitie 3, 820 <br />
                                84100 Ylivieska <br />
                                Finland <br />
                                Nadaasi <br />
                                Registered Name / ID: <br />
                                SUR-MEK AY / 2806255-3 <br />
                            </p>
                        </div>
                        <div className="company-info-4">
                            <img src={CallIcon} alt="Phone no." />
                        </div>
                        <div className="company-info-5">
                            <p>
                                Call us: <br />
                                046 9375 029
                    </p>
                        </div>
                        <div className="company-info-6">
                            <img src={MailIcon} alt="E-mail address" />
                        </div>
                        <div className="company-info-7">
                            <p>info@nadaasi.fi</p>
                        </div>
                    </div>
                    <div className="contact-form">
                        <form onSubmit={this.handleSubmitFeedback}>
                                <input
                                    className="text"
                                    type='text'
                                    name='name'
                                    placeholder="Name"
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                    required
                                />
                                
                                <input
                                    className="text"
                                    type='subject'
                                    name='subject'
                                    placeholder="Subject"
                                    value={this.state.subject}
                                    onChange={this.handleChange}
                                    required
                                />
                                <input
                                    className="text"
                                    type='email'
                                    name='email'
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                    required
                                />
                                <textarea
                                    className="textarea"
                                    name='message'
                                    placeholder="Message"
                                    value={this.state.message}
                                    onChange={this.handleChange}
                                    required
                                />
                                <br/>
                                <input type="submit" value="SEND" className="message-button"/>
                        </form>
                    </div>
                    <div className="contact-cards">
                        <div>
                            <div className="card-image">
                                <img src={Support} alt="Personalized Help" />
                                <h4><u>Personalized Help</u></h4>
                            </div>
                            <div className="card-texts">
                                <p>
                                    Whether you need help in choosing a right
                                    dress for special setting or you want to
                                    customize a dress - we can help. Contact us.
                        </p>
                            </div>
                        </div>
                        <div>
                            <div className="card-image">
                                <img src={Suggestion} alt="Suggestions" />
                                <h4><u>Suggestions</u></h4>
                            </div>
                            <div className="card-texts">
                                <p>
                                    We would really appreciate your feedback on
                                    what we have or how it could be better.
                        </p>
                            </div>
                        </div>
                        <div>
                            <div className="card-image">
                                <img src={JoinUs} alt="Join Us" />
                                <h4><u>Join Us</u></h4>
                            </div>
                            <div className="card-texts">
                                <p>
                                    If you have a dream to leave your mark on
                                    fashion industry, contact us. We take every
                                    initiative seriously, if your concepts meet
                                    our values, you will see them a reality and
                                    that is a promise.
                        </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact