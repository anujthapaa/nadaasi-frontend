import React, { Component } from 'react';

import '../styles/Home.css';
import FacebookLogo from '../images/home/icons/facebook.svg';
import InstagramLogo from '../images/home/icons/instagram.svg';
import { Card } from 'react-bootstrap';

import ShipIcon from '../images/home/icons/ship.svg';
import MoneyIcon from '../images/home/icons/money-back.svg';
import HoursIcon from '../images/home/icons/24-hours.svg';
import ShirtIcon from '../images/home/icons/t-shirt.svg';
import BrushIcon from '../images/home/icons/paint-brush.svg';
import CottonIcon from '../images/home/icons/cotton.svg';

const one = require('../images/home/n1.jpg');
const two = require('../images/home/n2.jpg');
const three = require('../images/home/n3.jpg');

export default class Home extends Component<any, any> {
    constructor(props) {
        super(props);

        this.state = {
            // position in carousel
            imagePostion: 1,
            background_image: three //default image in start
        }
    }

    componentDidMount() {
        setInterval(() => {
            //calls every 4000 miliseconds
            this.changeCarousel();
        }, 4000)
    }

    changeCarousel() {
        if (this.state.imagePostion === 1) {
            this.setState({
                imagePostion: 2,
                background_image: two
            })
        } else
        if (this.state.imagePostion === 2) {
            this.setState({
                imagePostion: 3,
                background_image: three
            })
        } else
        if (this.state.imagePostion === 3) {
            this.setState({
                imagePostion: 1,
                background_image: one
            })
        }
    }

    // nextCarousel() {
    //     setTimeout(() => {
    //         this.setState({
    //             background_image: two
    //         })
    //         this.prevCarousel()
    //     }, 4000);
    // }

    // prevCarousel() {
    //     setTimeout(() => {
    //         this.setState({
    //             background_image: one
    //         })
    //         this.nextCarousel()
    //     }, 4000);
    // }

    render() {
        return (
            <div className="wrapper">
                <div className="wrapper-item carousel"
                    ref="carousel"
                    style={{
                        backgroundImage: 'url(' + this.state.background_image + ')',
                        backgroundRepeat: 'no-repeat',
                        backgroundPositionX: 'center',
                        backgroundSize: 'cover',
                        backgroundColor: '#E9E9E9',
                        height: '98vh'
                    }}>
                    <div>
                        <h3>SIMPLE FABRIC WEDDING DRESSES</h3>
                        <h1>Beautiful fabrics last; synthetics don't.</h1>
                        <button><span>SHOP NOW</span></button>
                    </div>
                </div>

                <div className="wrapper-item home-section-textiles two">
                    <div>
                        <div className="home-section-textile">
                            <h1>Organic Textiles</h1>
                        </div>
                        <div className="home-section-textile">
                            <h3>For a better future</h3>
                        </div>
                        <div className="home-section-textile">
                            <p>
                                We're and eco-friendly company certified by Control Union certifications with
                                GOTS and Organic Content Standard ( Organic 100 abd Organic Blended) and able
                                to produce a wide range of organic fabric clothing. We located in Finland,
                                one of the northern regions in the world in high quality organic fabric production.
                    </p>
                        </div>
                        <div className="home-section-textile">
                            {/* <ButtonGroup>
                        <Button>SHOP NOW</Button>
                        <Button className="button-arrow"></Button>
                    </ButtonGroup> */}
                            <button><span>SHOP NOW</span></button>
                        </div>
                    </div>
                </div>

                <div className="wrapper-item three">
                    <h1>We Provide</h1>
                    <h3>better qualities</h3>
                    <div className="cards">
                        <Card>
                            <Card.Img src={ShirtIcon} />
                            <Card.Body>
                                <Card.Text>
                                    Textile Standard
                    </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img src={BrushIcon} />
                            <Card.Body>
                                <Card.Text>
                                    Standard Color
                    </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img src={CottonIcon} />
                            <Card.Body>
                                <Card.Text>
                                    Standard Cotton
                    </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    {/* <div className="cards">
                <div className="card1 t-shirt">
                    <h4><u>Textile Standard</u></h4>
                </div>
                <div className="card1 paint-brush">
                    <h4><u>Standard Color</u></h4>
                </div>
                <div className="card1 cotton">
                    <h4><u>Standard Cotton</u></h4>
                </div>
            </div> */}
                </div>

                <div className="wrapper-item newsletter">
                    <div>
                        <h3>
                            Sign up for our newsletter and get
                    <h1> 20% </h1>
                            discount on evening dresses
                </h3>
                        <input type="text" placeholder="Your Email Address" />
                        <button> <u> SUBSCRIBE </u> </button>
                    </div>
                </div>

                <div className="wrapper-item services">
                    <h1>Our Services</h1>
                    <h3>better qualities</h3>
                    <div className="cards">
                        <Card>
                            <Card.Img src={ShipIcon} />
                            <Card.Body>
                                <Card.Text>
                                    Free Shipping Worldwide
                    </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img src={MoneyIcon} />
                            <Card.Body>
                                <Card.Text>
                                    Money Back Guarantee
                    </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img src={HoursIcon} />
                            <Card.Body>
                                <Card.Text>
                                    24/7 Customer Support
                    </Card.Text>
                            </Card.Body>
                        </Card>
                        {/* <div className="card1 ship">
                    <h4><u>Free Shipping Worldwide</u></h4>
                </div>
                <div className="card1 money-back">
                    <h4><u>Money Back Guarantee</u></h4>
                </div>
                <div className="card1 hours">
                    <h4><u>24/7 Customer Support</u></h4>
                </div> */}
                    </div>
                </div>

                <div className="wrapper-item social">
                    <div className="social-item-one">

                    </div>
                    <div className="social-item-two">

                    </div>
                    <div className="social-item-three">
                        <p>
                            Follow us on <br /><br />
                            <img src={FacebookLogo} width="25px" alt="facebook" />
                            <img src={InstagramLogo} width="25px" alt="instagram" />
                        </p>
                    </div>
                    <div className="social-item-four">

                    </div>
                    <div className="social-item-five">

                    </div>
                    <div className="social-item-six">

                    </div>
                </div>

                <div className="wrapper-item review">
                    <div className="review-card">
                        <div className="review-header">
                            <p>
                                It's so elegant and stunning
                        </p>
                            <ul className="stars">
                                <li className="star"></li>
                                <li className="star"></li>
                                <li className="star"></li>
                                <li className="star"></li>
                                <li className="star"></li>
                            </ul>
                        </div>
                        <p className="review-text">
                            This dress was wonderful! The fit was perfect, the shape was fabulous,
                            and corset back was a gorgeous touch. I will say the bottom was a tad long,
                        but nothing a tiny hem couldn't fix! I recommend this 100%.</p>
                        <p className="review-name">by Angela de Silva</p>
                    </div>

                </div>
            </div >
        )
    }
}