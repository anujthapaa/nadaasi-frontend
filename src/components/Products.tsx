import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Shop.css';

export default class Products extends Component<{
    products, sort,
    handleChangeSort, handleAddToCart, handleDetail
}> {

    ellipsify = (str) => {
        if (str.length > 10) {
            return (str.substring(0, 10) + "...");
        }
        else {
            return str;
        }
    }
    render() {
        // const { _id, name, price, availableSizes, dressType } = this.props.products;



        const productItems = this.props.products.map(product => (
            <div>
                <div className="product" key={product._id}
                    onClick={(e) => this.props.handleDetail(e, product)}>
                    <img src={require(`../images/Shop/${product.Product_id}.jpg`)} alt="Product" />
                    <div className="product-description">
                        <h5 className="product-name">
                            {product.name}
                        </h5>
                        <div>
                            <p className="product-price">
                                €{product.price}
                            </p>
                            <ul className="product-rating stars">
                                <li className="star"></li>
                                <li className="star"></li>
                                <li className="star"></li>
                                <li className="star"></li>
                                <li className="star"></li>
                            </ul>
                        </div>
                        {/* <div>
                                <button onClick={(e) => this.props.handleAddToCart(e, product)}>Add to Cart</button>
                            </div> */}
                    </div>
                </div>
            </div>

        )
        )

        return (
            <div>
                <div className="products-sort">
                    <div className="product-display">
                        <img src={require('../images/Shop/icons/display.png')} alt="display"/>
                    </div>
                    <div className="product-count" style={{ 'fontFamily': "FuturaLtBT" }}>
                        {` Showing ${this.props.products.length} products.`}
                    </div>
                    <div className="product-sort">
                        <p>Sort by: </p>
                        <select
                            className="form-control"
                            value={this.props.sort}
                            onChange={this.props.handleChangeSort}
                        >
                            <option value="">Select</option>
                            <option value="lowestprice">lowest to highest</option>
                            <option value="highestprice">highest to lowest</option>
                        </select>
                    </div>

                </div>
                <div className="products">
                    {productItems}
                </div>
            </div>
        )
    }
}