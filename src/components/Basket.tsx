import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../styles/index.css';

import KlarnaLogo from '../images/Cart/banks/klarna_logo_black.png';
import OPLogo from '../images/Cart/banks/osuuspankki.svg';
import DanskeLogo from '../images/Cart/banks/danske-bank.svg';
import AktiaLogo from '../images/Cart/banks/aktia.svg';
import VisaLogo from '../images/Cart/banks/visa.svg';
import NordeaLogo from '../images/Cart/banks/nordea.svg';
import SLogo from '../images/Cart/banks/s-pankki.svg';
import SaastoLogo from '../images/Cart/banks/saastopankki.svg';
import MasterCardLogo from '../images/Cart/banks/mastercard.svg';
import DeleteIcon from '../images/Cart/delete.svg';
import OrderIcon from '../images/Cart/order.svg';
import { InputGroup, Button } from 'react-bootstrap';

// history component for routing to desired route
class Basket extends Component<{ history, loggedInStatus, user, handleCart, handleRemoveFromCart }, any> {

    constructor(props) {
        super(props);

        this.state = {
            cartItems: [],
            total_with_vat: 0,
            vat: 0,
            // address: [],
            address: {},
            country: '',
            city: '',
            postal_code: '',
            street: '',
            message: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.checkout = this.checkout.bind(this);
        this.handleCart = this.handleCart.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    componentDidMount() {
        this.handleRefresh();
    }
    
    handleRefresh() {
        if (localStorage.getItem('cartItems')) {
            // if(this.props.cartItems) {
            const data = JSON.parse(localStorage.getItem('cartItems'));
            const cartItems = JSON.parse(localStorage.getItem('cartItems'));
    
            let initialValue = 0;
            let total_with_vat = cartItems.reduce(function (a, c) {
                console.log(a, c.price * c.count);
    
                return a + c.price * c.count
            }, initialValue);
    
            let vat = total_with_vat / 20;
    
            if (cartItems.length > 0) {
                this.setState({
                    cartItems: cartItems,
                    total_with_vat: total_with_vat,
                    vat: vat
                });
            }
        }
    }

    handleChange = async (event) => {
        event.preventDefault();
        // const name = event.target.name;
        // const value = event.target.value;
        const { name, value } = event.target;
        await this.setState({ [name]: value }, () => {
            console.log([name], value)
        })

        console.log(
            "country-" + this.state.country,
            " city-" + this.state.city,
            " postal-" + this.state.postal_code,
            " street-" + this.state.street,
        );
    }

    handleCart = (cartItems) => {
        this.props.handleCart(cartItems);
        this.setState({ cartItems: cartItems });
    }

    handleRemove = async (e, cartItem) => {
        const cartItems = this.state.cartItems;

        await this.setState(state => {
            const itemToRemove = cartItems.filter(a => a.addedItem._id === cartItem.addedItem._id);
            console.log(itemToRemove);


            if (itemToRemove[0].count > 1) {
                itemToRemove[0].count -= 1
                console.log("decreasing count", itemToRemove);

            } else if (itemToRemove[0].count === 1) {
                console.log("removing", itemToRemove);
                const itemIndex = cartItems.findIndex(e => e.addedItem._id === itemToRemove[0].addedItem._id);
                if (itemIndex > -1) {
                    cartItems.splice(itemIndex, 1);
                }
                console.log("removed", itemToRemove, cartItems);

            }
        });

        localStorage.setItem('cartItems', JSON.stringify(cartItems));
        this.handleRefresh();
        this.handleCart(cartItems);
        return { cartItems: cartItems };
    }

    toLogin = () => {
        // redirecting to profile page if not loggedin
        const { history } = this.props;
        history.push('/profile');
    }

    checkout = async () => {
        const {
            country, city, postal_code, street,
            message, total_with_vat,
            cartItems, address } = this.state

        address.country = country;
        address.city = city;
        address.postal_code = postal_code;
        address.street = street;
        console.log(typeof (address));


        const orderOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                cartItems: cartItems,
                address: address,
                message: message,
                total: total_with_vat
            })
        };

        await fetch("http://localhost:8000/order", orderOptions)
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    // redirecting to shop page if loggedin and order recieved
                    const { history } = this.props;
                    history.push('/shop');
                    alert("Order recieved");
                    localStorage.clear();
                }
            })
            .catch(error => {
                console.log("feedback input error", error);
            })
    }

    render() {
        const { user, loggedInStatus } = this.props;
        const { cartItems, total_with_vat, vat } = this.state;

        return (
            <div className="basket" style={{ 'fontFamily': "FuturaMdBT" }}>
                <div className="cart-image">
                    <div className="cart-image-text">
                        <h1>CART</h1>
                    </div>
                </div>

                {cartItems.length === 0
                    ? "Basket is empty" :
                    <div>You have {cartItems.length} items in the basket. <hr /></div>
                }
                {cartItems.length > 0 &&
                    <div className="cart">
                        <div className="cart-desc">
                            <h3>SHOPPING CART</h3>
                            <ul className="cart-items" style={{ marginLeft: -25 }}>
                                {cartItems.map(item => (
                                    <li className="cart-item" key={item.addedItem.id}>
                                        <div className="cart-item-image">
                                            <img src={require(`../images/Shop/FP_MG_3537-Edited.jpg`)} alt="product" width="100" />
                                        </div>
                                        <div className="cart-item-name">
                                            <b>{item.addedItem.name}</b><br />
                                            <p> € {item.addedItem.price}</p>
                                        </div>
                                        <div className="cart-item-count">
                                            {item.count}
                                        </div>
                                        <a href="">{item[1]}</a>
                                        <a className="cart-item-delete" onClick={(e) => this.handleRemove(e, item)}>
                                            <img src={DeleteIcon} alt="delete" width="40" />
                                        </a>
                                    </li>))
                                }
                            </ul>
                            <br /><br />
                            <InputGroup>
                                <img src={OrderIcon} className="coupon-image" alt="order icon" />
                                <input className="coupon-input" type="number" placeholder="COUPON CODE" />
                                <InputGroup.Append>
                                    <Button className="coupon-button" variant="outline-secondary">APPLY</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <div>
                                <br />
                                <label className="checkbox-container">Subscribe to our newsletter
                                    <input type="checkbox" />
                                    <span className="checkmark"></span>
                                </label>

                            </div>
                        </div>
                        <div className="cart-total">
                            <h3 style={{ gridColumn: '1 /span 2', marginBottom: '2em' }}>CART TOTAL</h3>
                            <p>SUBTOTAL</p>
                            <p>{(total_with_vat - vat).toFixed(2)}</p>
                            <p>SHIPPING</p>
                            <p>Free Shipping to Finland</p>
                            <p>ADDRESS*</p>
                            <div className="delivery_address">
                                <select
                                    name="country"
                                    onChange={this.handleChange}
                                    defaultValue="finland"
                                    className="delivery-input">
                                    <option value="finland">Finland</option>
                                    <option value="sweden">Sweden</option>
                                    <option value="denmark">Denmark</option>
                                    <option value="norway">Norway</option>
                                </select>
                                <input
                                    className="delivery-input"
                                    type="text"
                                    name="city"
                                    placeholder="Town / City"
                                    value={this.state.city}
                                    onChange={this.handleChange}
                                    required
                                />
                                <input
                                    className="delivery-input"
                                    type="number"
                                    name="postal_code"
                                    placeholder="Postal Code"
                                    value={this.state.postal_code}
                                    onChange={this.handleChange}
                                    required
                                />
                                <input
                                    className="delivery-input"
                                    type="text"
                                    name="street"
                                    placeholder="Street Address"
                                    value={this.state.street}
                                    onChange={this.handleChange}
                                    required
                                />
                            </div>
                            <div style={{ gridColumn: '1 /span 2', marginBottom: '2em' }}>
                                <p>MESSAGE</p>
                                <textarea
                                    name="message"
                                    onChange={this.handleChange}
                                    value={this.state.message}
                                    style={{ width: '100%' }}></textarea>
                            </div>
                            <div className="delivery-subtotal">
                                <div>
                                    <p>SUBTOTAL</p>
                                    <h5>€  {(total_with_vat - vat).toFixed(2)}</h5>
                                </div>
                                <div>
                                    <p>VAT</p>
                                    <h5>€  {vat.toFixed(2)}</h5>
                                </div>
                            </div>
                            <div className="delivery-total">
                                <h5>€ {total_with_vat.toFixed(2)}</h5>
                            </div>
                            {loggedInStatus === "LOGGED_IN"
                                ? <button className="checkout-button" onClick={this.checkout}>PROCEED TO CHECKOUT</button>
                                : <button className="checkout-button" onClick={this.toLogin}>PROCEED TO CHECKOUT</button>}
                            <p className="checkout-terms">
                                By clicking "Proceed to Checkout" I approve Klarna's terms User
                                terms and confirm I have read Klarna's Privacy Notice I agree to
                                the terms and condition of SUR-MEK avoin yhtiö
                            </p>
                            <p style={{ gridColumn: '1 /span 2', marginBottom: '0' }}>
                                Available Payment Methods
                            </p>
                            <p className="checkout-terms">
                                Pay later, monthly financing, Bank transfer and card
                            </p>
                            <div style={{ gridColumn: '1 /span 2', marginBottom: '2em' }}>
                                <img src={KlarnaLogo} alt="User" width="40" />
                                <img src={OPLogo} alt="User" width="40" />
                                <img src={DanskeLogo} alt="User" width="40" />
                                <img src={AktiaLogo} alt="User" width="40" />
                                <img src={VisaLogo} alt="User" width="40" />
                                <img src={NordeaLogo} alt="User" width="40" />
                                <img src={SLogo} alt="User" width="40" />
                                <img src={SaastoLogo} alt="User" width="40" />
                                <img src={MasterCardLogo} alt="User" width="40" />
                            </div>

                        </div>
                    </div>
                }

            </div>
        )
    }
}

export default withRouter(Basket);