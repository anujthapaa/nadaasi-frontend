import React, { Component } from 'react'
import { Form } from 'react-bootstrap';

class ProductInsert extends Component<{ edit, productDetail }, any> {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.productDetail._id,
            updatedName: this.props.productDetail.name,
            updatedDetail: this.props.productDetail.detail,
            updatedPrice: this.props.productDetail.price,
            updatedAvailableSizes: this.props.productDetail.availableSizes,
            updatedDressType: this.props.productDetail.DressType,
            name: '',
            detail: '',
            price: '',
            availableSizes: [],
            dressType: '',
        }
        this.handleChangeInputName = this.handleChangeInputName.bind(this);
        this.handleChangeInputDetail = this.handleChangeInputDetail.bind(this);
        this.handleChangeInputPrice = this.handleChangeInputPrice.bind(this);
        this.handleChangeInputDressType = this.handleChangeInputDressType.bind(this);
        this.handleChangeInputSize = this.handleChangeInputSize.bind(this);
        this.handleChangeUpdateName = this.handleChangeUpdateName.bind(this);
        this.handleChangeUpdateDetail = this.handleChangeUpdateDetail.bind(this);
        this.handleChangeUpdatePrice = this.handleChangeUpdatePrice.bind(this);
        this.handleChangeUpdateDressType = this.handleChangeUpdateDressType.bind(this);
        this.handleChangeUpdateSize = this.handleChangeUpdateSize.bind(this);
    }

    handleChangeInputName = async event => {
        const name = event.target.value
        await this.setState({ name })
    }
    handleChangeInputDetail = async event => {
        const detail = event.target.value
        await this.setState({ detail })
    }
    handleChangeInputPrice = async event => {
        const price = event.target.value
        await this.setState({ price })
    }
    handleChangeInputDressType = async event => {
        await this.setState({ dressType: event.target.value });
    }
    handleChangeInputSize = async event => {
        const options = event.target.options;
        const sizes = [];

        for (let i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                await sizes.push(options[i].value);
            }
        }
        this.setState({ availableSizes: sizes });
        console.log(this.state.availableSizes);

    }
    handleChangeUpdateName = async event => {
        const updatedName = event.target.value;
        await this.setState({ updatedName: updatedName })
        console.log(this.state.updatedName);

    }
    handleChangeUpdateDetail = async event => {
        const updatedDetail = event.target.value
        await this.setState({ updatedDetail })
    }
    handleChangeUpdatePrice = async event => {
        const updatedPrice = event.target.value
        await this.setState({ updatedPrice })
    }
    handleChangeUpdateDressType = async event => {
        await this.setState({ dressType: event.target.value });
    }
    handleChangeUpdateSize = async event => {
        const options = event.target.options;
        const sizes = [];

        for (let i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                await sizes.push(options[i].value);
            }
        }
        this.setState({ availableSizes: sizes });
        console.log(this.state.availableSizes);

    }

    handleUpdateProduct = async () => {
        const { id, updatedName, updatedDetail, updatedPrice, updatedDressType, updatedAvailableSizes } = this.state
        const updateOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: updatedName,
                detail: updatedDetail,
                price: updatedPrice,
                DressType: updatedDressType,
                availableSizes: updatedAvailableSizes
            })
        };

        await fetch("http://localhost:8000/nadassi/" + id, updateOptions)
            .then(res => res.json())
            .then(res => console.log(res));
    }

    handleSubmitProduct = async () => {
        const { name, detail, price, dressType, availableSizes } = this.state
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                detail: detail,
                price: price,
                DressType: dressType,
                availableSizes: availableSizes
            })
        };

        await fetch("http://localhost:8000/nadassi", options)
            .then(res => res.json())
            .then(res => console.log(res))
    }

    render() {
        const edit = this.props.edit; //To determine if the input form is for edit or insert
        const { name, detail, price, 
            updatedName, updatedDetail, updatedPrice, 
            updatedAvailableSizes, updatedDressType } = this.state;

        return (
            <div>
                {edit ?
                    <Form>
                        Name: <input type="text" value={updatedName} onChange={this.handleChangeUpdateName} />
                        Detail: <input type="text" value={updatedDetail} onChange={this.handleChangeUpdateDetail} />
                        Price: <input type="number" value={updatedPrice} min="0" onChange={this.handleChangeUpdatePrice} />
                        Available Sizes (Press ctrl and click for multiple select)
                        <select multiple onChange={this.handleChangeUpdateSize}>
                            {updatedAvailableSizes.map( item => (
                                <option value={item}>{item}</option>
                            ))}
                            {/* <option value="xs">Extra Small</option>
                            <option value="s">Small</option>
                            <option value="m">Medium</option>
                            <option value="l">Large</option>
                            <option value="xl">Extra Large</option>
                            <option value="xxl">Double Extra Large</option> */}
                        </select><br />
                        Dress Type
                        <select value={this.state.DressType} onChange={this.handleChangeUpdateDressType}>
                            <option value="casual">Casual</option>
                            <option value="evening">Evening</option>
                        </select>
                        <br />
                        <button onClick={this.handleUpdateProduct}>Update</button>
                        <br />
                    </Form> : <Form>

                        Name: <input type="text" value={name} onChange={this.handleChangeInputName} />
                        Detail: <input type="text" value={detail} onChange={this.handleChangeInputDetail} />
                        Price: <input type="number" value={price} min="0" onChange={this.handleChangeInputPrice} />

                        Available Sizes (Press ctrl and click for multiple select)
                        <select multiple onChange={this.handleChangeInputSize}>
                            <option value="xs">Extra Small</option>
                            <option value="s">Small</option>
                            <option value="m">Medium</option>
                            <option value="l">Large</option>
                            <option value="xl">Extra Large</option>
                            <option value="xxl">Double Extra Large</option>
                        </select><br />
                        Dress Type
                        <select value={this.state.DressType} onChange={this.handleChangeInputDressType}>
                            {}
                            <option value="casual">Casual</option>
                            <option value="evening">Evening</option>
                        </select>
                        <br />
                        <button onClick={this.handleSubmitProduct}>Submit</button>
                        <br />
                    </Form>
                }
            </div>
        )
    }
}

export default ProductInsert