import React, { Component } from 'react'
import { Form } from 'react-bootstrap';
import { Redirect } from 'react-router';

const validEmailRegex =
    RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

const validateForm = (errors) => {
    let valid = true;
    Object.values(errors).map(
        // if we have an error string set valid to false
        (val) => (valid = true)
    );
    return valid;
}

const countErrors = (errors) => {
    let count = 0;
    Object.keys(errors).map(
        (val) => val.length > 0 && (count = count + 1)
    );
    return count;
}

class Signup extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            // password_confirmation: '',
            redirect: false,
            formValid: false,
            errorCount: null,
            errors: {
                username: '',
                email: '',
                password: ''
            }
        };

        this.handleSubmitUser = this.handleSubmitUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        event.preventDefault();
        // const name = event.target.name;
        // const value = event.target.value;
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'username':
                errors.username =
                    value.length < 5
                        ? 'Username must be at least 5 characters long'
                        : '';
                break;
            case 'email':
                errors.email =
                    validEmailRegex.test(value)
                        ? ''
                        : 'Email is not valid';
                break;
            case 'password':
                errors.password =
                    value.length < 8
                        ? 'Password must be at least 8 characters long'
                        : '';
                break;
            default:
                break;

        }

        this.setState({ errors, [name]: value }, () => {
            console.log(errors)
        })
    }

    handleSubmitUser = async (event) => {
        const { username, email, password } = this.state

        this.setState({ formValid: validateForm(this.state.errors) });
        this.setState({ errorCount: countErrors(this.state.errors) });

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                email: email,
                password: password,
                role: "guest"
            })
        };

        console.log(this.state.formValid);

        // if( this.state.formValid === true) {
        await fetch("http://localhost:8000/user", options)
            .then(res => console.log(res))
            .catch(error => {
                console.log("registration error", error);
            })

        this.setState({ redirect: true })
        // }
        // event.preventDefault();
    }

    render() {
        const { errors, formValid } = this.state;

        if( this.state.redirect === true) {
            return <Redirect to="/"/>
        }

        return (
            <div>
                {/* <Form onSubmit={this.handleSubmitUser} noValidate>
                    Username: <input name="username" type="text" value={username} onChange={this.handleChange} />
                    Email: <input name="email" type="text" value={email} onChange={this.handleChange} />
                    Password: <input name="password" type="password" value={password} onChange={this.handleChange} />
                    Confirm Password: <input type="password" value={password} onChange={this.handleChange} />
                    <br />
                    <button onClick={this.handleSubmitUser}>Sign Up</button>
                    <br />
                </Form> */}

                <div className='form-wrapper mx-auto w-50'>
                    <h2>Create Account</h2>
                    <Form onSubmit={this.handleSubmitUser}>
                        <div className='username'>
                            <label htmlFor="username">Username</label>
                            <input
                                type='text'
                                name='username'
                                placeholder="Username"
                                value={this.state.username}
                                onChange={this.handleChange}
                                required
                            />
                            {errors.username.length > 0 &&
                                <span className='error'>{errors.username}</span>}
                        </div>
                        <div className='email'>
                            <label htmlFor="email">Email</label>
                            <input
                                type='email'
                                name='email'
                                placeholder="Email"
                                value={this.state.email}
                                onChange={this.handleChange}
                                required
                            />
                            {errors.email.length > 0 &&
                                <span className='error'>{errors.email}</span>}
                        </div>
                        <div className='password'>
                            <label htmlFor="password">Password</label>
                            <input
                                type='password'
                                name='password'
                                placeholder="Password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                required
                            />
                            {errors.password.length > 0 &&
                                <span className='error'>{errors.password}</span>}
                        </div>
                        {/* <div className='password'>
                            <label htmlFor="password">Password Confirmation</label>
                            <input 
                                type='password' 
                                name='password'
                                placeholder="Password Confirmation"
                                value={this.state.password_confirmation} 
                                onChange={this.handleChange} 
                                required
                                />
                            {errors.password.length > 0 &&
                                <span className='error'>{errors.password}</span>}
                        </div> */}
                        <div className='info'>
                            <small>Password must be eight characters in length.</small>
                        </div>
                        <div className='submit'>
                            <button type="submit">Create</button>
                        </div>
                        {this.state.errorCount !== null ?
                            <p className="form-status">
                                Form is {formValid ? 'valid ✅' : 'invalid ❌'}
                            </p> : ''}
                    </Form>
                </div>
            </div>
        );
    }
}

export default Signup