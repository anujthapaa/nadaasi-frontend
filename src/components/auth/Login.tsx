import React, { Component } from "react";
export default class Login extends Component<{ handleSuccessfulAuth }, any> {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      // loginErrors: "",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { email, password } = this.state;

    const loginOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }

    fetch("http://localhost:8000/user/login", loginOptions)
      .then(res => res.json())
      .then(data => {
        data ? this.props.handleSuccessfulAuth(data)
        : console.log("no data");
      })
      .then(data =>
        this.setState({
          user: data
        }))
      .catch(error => {
        console.log("login error", error);
      })

    event.preventDefault();
  }

  render() {
    return (
      <div className="login_form">
        <form onSubmit={this.handleSubmit}>
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChange}
            required
          />

          <input
            type="password"
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleChange}
            required
          />

          <button type="submit">Login</button>
        </form>
      </div>
    )
  }
}