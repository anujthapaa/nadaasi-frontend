import React, { Component } from 'react';

import Login from './auth/Login';
// import Signup from './auth/Signup';

import ProductInsert from './ProductInsert';
// import Products from './Products';

import '../styles/Profile.css';
import { Table, Tabs, Tab } from 'react-bootstrap';

class Profile extends Component<{ handleLogin, handleLogout, loggedInStatus, isAdmin }, any> {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            feedbacks: [],
            orders: [],
            user: '',
            username: '',
            email: '',
            password: '',
            showProductInsert: false,
            products: [],
            showDetail: false,
            productDetail: [],
            edit: false,
            id: null,
            userId: '',
            activeTab: 'orders'
        };
        this.toggleProductInsert = this.toggleProductInsert.bind(this);
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this);
        this.userLogout = this.userLogout.bind(this);
    }

    handleSuccessfulAuth(data) {
        this.props.handleLogin(data);

        if (this.props.loggedInStatus === "LOGGED_IN" && data.role === 'admin') {
            this.setState({
                // Checks if the role is admin to display the admin dashboard
                // adminDashboard: true,
                user: data
            })
        }

        // this.props.history.push("/");        
    }
    componentDidUpdate() {
        console.log(this.props.loggedInStatus);

    }
    componentDidMount() {
        this.setState({
            // Checks if the role is admin to display the admin dashboard
            // adminDashboard: true 
        })

        console.log(this.props.loggedInStatus, this.state.user.role, this.props.isAdmin);

    }

    async handleSelect(selectedTab) {
        // The active tab must be set into the state so that
        // the Tabs component knows about the change and re-renders.
        console.log(selectedTab);

        if (selectedTab === 'products') {
            this.listProducts()
        } else if (selectedTab === 'users') {
            this.listUsers()
        } else if (selectedTab === 'feedbacks') {
            this.listFeedbacks()
        } else {
            this.listOrders()
        }

        await this.setState({
            activeTab: selectedTab
        });

    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleChangeUsername = async event => {
        const username = event.target.value
        await this.setState({ username })
    }
    handleChangePassword = async event => {
        const password = event.target.value
        await this.setState({ password })
    }

    callAPI() {
        fetch("http://localhost:8000/nadassi")
            .then(res => res.json())
            .then(data =>
                this.setState({
                    products: data
                }) // Stores the result to products component
            );
    }

    listProducts = async () => {
        await this.callAPI();
    }

    listUsers = () => {
        fetch("http://localhost:8000/user")
            .then(res => res.json())
            .then(data =>
                this.setState({
                    users: data
                }) // Stores the result to products component
            );
    }

    listFeedbacks = async () => {
        await fetch("http://localhost:8000/feedback")
            .then(res => res.json())
            .then(data =>
                this.setState({
                    feedbacks: data,
                }) // Stores the result to products component
            );
    }

    listOrders = async () => {
        await fetch("http://localhost:8000/order")
            .then(res => res.json())
            .then(data =>
                this.setState({
                    orders: data,
                }) // Stores the result to products component
            );

        console.log("Orders", this.state.orders);
        
    }

    deleteProduct = async id => {
        await fetch("http://localhost:8000/nadassi/" + id, { method: 'DELETE' })
            .then(res => console.log(res))

        this.callAPI();
    }

    updateProduct = async (e, product) => {
        await this.setState({
            productDetail: product,
            edit: !this.state.edit,
            showProductInsert: !this.state.showProductInsert
        })
    }

    toggleProductInsert() {
        this.setState({
            showProductInsert: !this.state.showProductInsert,
            edit: false
        });
    }

    updateUser = async (e, user) => {
        console.log(user);
    }
    userLogout = () => {
        // this.setState({
        //     user: "",
        //     adminDashboard: false,
        //     isAuthenticated: false
        // })
        this.props.handleLogout();
    }

    render() {
        const { username, email, password } = this.state;
        const { loggedInStatus, isAdmin } = this.props;

        const users = this.state.users.map(user => (
            <tr key={user._id}>
                <td>{this.state.users.indexOf(user)}</td>
                <td>{user.username}</td>
                <td>{user.email}</td>
            </tr>
        ))

        const feedbacks = this.state.feedbacks.map(feedback => (
            <tr key={feedback._id}>
                <td>{this.state.feedbacks.indexOf(feedback)}</td>
                <td>{feedback.name}</td>
                <td>{feedback.subject}</td>
                <td>{feedback.email}</td>
                <td>{feedback.message}</td>
            </tr>
        ))

        const orders = this.state.orders.map(order => (
            <tr key={order._id}>
                <td>{this.state.orders.indexOf(order)}</td>
                <td>
                    <ul>
                        {order.cartItems.map(item => (
                            <li>{item.addedItem.name}</li>
                        ))}
                    </ul>
                </td>
                <td>{order.message}</td>
                <td>{order.address.street}</td>
                <td>{order.total}</td>
            </tr>
        ))

        const productItems = this.state.products.map(product => (
            <tr key={product._id}>
                <td>{this.state.products.indexOf(product)}</td>
                <td>{product.name}</td>
                <td>{product.detail}</td>
                <td>{product.price}</td>
                <td><button onClick={(e) => this.updateProduct(e, product)}>Edit</button></td>
                <td><button onClick={(e) => this.deleteProduct(product.name)}>Delete</button></td>
            </tr>
        )
        )

        return (
            <div className="dashboard">
                {/* <Logged in state provides user services /> */}
                {loggedInStatus === "LOGGED_IN" &&
                    <div>
                        {/* adminDashboard checks if the logged in user is admin then displays admin dashboard */}
                        {isAdmin ?
                            <div>
                                <h1>{username}</h1>
                                <br />
                                {this.state.showProductInsert
                                    ? <div>
                                        <button className="button-close-product-insert" onClick={this.toggleProductInsert}>Cancel</button>
                                        <ProductInsert edit={this.state.edit} productDetail={this.state.productDetail} />
                                    </div>
                                    : <div>
                                        <button className="profile_button button_product_insert" onClick={this.toggleProductInsert}>New Product</button>
                                        <br /><br />
                                        <button className="profile_button button_product_insert" onClick={this.userLogout}>Log out</button>
                                        <br /><br />

                                        <Tabs id="controlled-tab-example" activeKey={this.state.activeTab} onSelect={this.handleSelect}>
                                            <Tab eventKey="products" title="Products" onClick={this.listProducts}>
                                                <h5>Total: {this.state.products.length}</h5>
                                                <Table responsive striped bordered hover>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Type</th>
                                                            <th>Price</th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {productItems}
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="users" title="Users">
                                                <h5>Total: {this.state.users.length}</h5>
                                                <Table responsive striped bordered hover>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {users}
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="feedbacks" title="Feedbacks">
                                                <h5>Total: {this.state.feedbacks.length}</h5>
                                                <Table responsive striped bordered hover>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Subject</th>
                                                            <th>Email</th>
                                                            <th>Message</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {feedbacks}
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="orders" title="Orders">
                                                <h5>Total: {this.state.orders.length}</h5>
                                                <Table responsive striped bordered hover>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Item</th>
                                                            <th>Message</th>
                                                            <th>Address</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {orders}
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                        </Tabs>
                                        <br /><br />
                                    </div>
                                }
                            </div> :
                            <div>
                                <h1>{username}</h1>
                                <br /><br />
                                <button onClick={(e) => this.updateUser(e, username)}>Edit</button>
                                <button onClick={this.userLogout}>Log out</button>
                            </div>
                        }
                    </div>
                }
                {/* Login form display when no user is logged in */}
                {loggedInStatus === "NOT_LOGGED_IN" &&
                    <div>
                        <br /><br />
                        <Login handleSuccessfulAuth={this.handleSuccessfulAuth} />
                        <br /><br />
                    </div>
                }
            </div>
        )
    }

}

export default Profile