import React, { Component } from 'react';
import { Link, Route, Switch, NavLink, BrowserRouter as Router } from 'react-router-dom';
import { Navbar, Nav, FormControl, Popover, OverlayTrigger } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/index.css';

import Home from './components/Home';
import Shop from './components/Shop';
import Contact from './components/Contact';
import About from './components/About';
import Basket from './components/Basket';
import Profile from './components/Profile';
import Dashboard from './components/Dashboard';
import Login from './components/auth/Login';
import Signup from './components/auth/Signup';

import logo from './images/nadaasi/Nadaasioriginal.png';
import invertlogo from './images/nadaasi/Nadaasi-white.png'
import UserLogo from './images/home/icons/user.svg';
import SearchLogo from './images/home/icons/search.svg';
import CartLogo from './images/home/icons/shopping-cart.svg';
import TelephoneLogo from './images/footer/telephone-white.svg';
import EnvvelopeLogo from './images/footer/envelope-white.svg';

class App extends Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      loggedInStatus: "NOT_LOGGED_IN",
      isAdmin: false,
      // isAuthenticated: false,
      user: {},
      cartItems: []
    }

    this.handleCart = this.handleCart.bind(this);
    this.changeLoginStatus = this.changeLoginStatus.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentDidMount() {
    this.checkLoginStatus();
    this.checkCartItems();
  }

  changeLoginStatus(loggedInStatus) {
    this.setState({ loggedInStatus: loggedInStatus })
  }

  checkLoginStatus() {
    // console.log(this.state.loggedInStatus);

    // if(this.state.loggedInStatus === "LOGGED_IN") {
    //   console.clear();
    //   console.log(this.state.loggedInStatus);
    // } else if(this.state.loggedInStatus === "LOGGED_OUT") {
    //   console.clear();
    //   console.log(this.state.loggedInStatus);
    // }

    // axios.get("http://localhost:8000/logged_in", { withCredentials: true })
    //   .then(response => {
    //     if (
    //       response.data.logged_in &&
    //       this.state.loggedInStatus === "NOT_LOGGED_IN"
    //     ) {
    //       this.setState({
    //         loggedInStatus: "LOGGED_IN",
    //         user: response.data.user
    //       });
    //     } else if (
    //       !response.data.logged_in &&
    //       (this.state.loggedInStatus === "LOGGED_IN")
    //     ) {
    //       this.setState({
    //         loggedInStatus: "NOT_LOGGED_IN",
    //         user: {}
    //       });
    //     }
    //   }).catch(error => {
    //     console.log("check login error", error);
    //   });
  }

  checkCartItems() {
    if (localStorage.getItem('cartItems')) {
      this.setState({ cartItems: JSON.parse(localStorage.getItem('cartItems')) });
    }
  }

  handleLogout() {
    this.setState({
      loggedInStatus: "NOT_LOGGED_IN",
      isAdmin: false,
      user: {}
    });
  }

  handleLogin(data) {
    this.setState({
      loggedInStatus: "LOGGED_IN",
      isAdmin: true,
      user: data
    });
  }

  handleCart = (cartItems) => {
    this.setState({ cartItems: cartItems })
  }

  render() {
    const searchInput = (
      <Popover id="popover-basic">
        <Popover.Content>
          <FormControl
            placeholder="Search"
            aria-label="Username"
            aria-describedby="basic-addon1"
          />
        </Popover.Content>
      </Popover>
    );
    return (
      <div className="App">
        <Router>
          {/* Navbar */}
          <header className="navigation">
            <Navbar expand="lg" className="navbar" bsPrefix="navbar" collapseOnSelect>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Brand href="#" className="logo">
                {/* <NavLink className="logo" to="/shop"> */}
                <img src={logo} alt="logo" width="200" />
                {/* </NavLink> */}
              </Navbar.Brand>
              <Navbar.Collapse id="basic-navbar-nav" className="">
                <Nav className="menu">
                  <NavLink className="menu-item" to="/" exact>Home</NavLink>
                  <NavLink className="menu-item" to="/shop">Shop</NavLink>
                  <NavLink className="menu-item" to="/about">About</NavLink>
                  <NavLink className="menu-item" to="/Contact">Contact</NavLink>
                </Nav>
              </Navbar.Collapse>
              <Navbar.Collapse id="basic-navbar-nav" className="">
                <Nav className="tools">
                  <NavLink to="/profile">
                    <a className="tool-item"><img src={UserLogo} alt="User" width="20" /></a>
                  </NavLink>

                  {/* Rootclose allows to close the overlay on clicking anywhere */}
                  <OverlayTrigger trigger="click" rootClose placement="bottom" overlay={searchInput}>
                    <a className="tool-item"><img src={SearchLogo} alt="Search" width="20" /></a>
                  </OverlayTrigger>

                  <NavLink to="/basket">
                    <a className="tool-item">
                      <img src={CartLogo} alt="Cart" width="20" />
                      <span className="badge">{this.state.cartItems.length}</span>
                    </a>
                  </NavLink>

                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </header>

          {/* Routes */}
          <Switch>
            <Route exact path={"/"}
              render={props => (
                <Home
                  {...props}
                  loggedInStatus={this.state.loggedInStatus}
                />
              )}
            />
            <Route exact path={"/dashboard"} render={props => (
              <Dashboard
                {...props}
                loggedInStatus={this.state.loggedInStatus}
              />
            )}
            />
            <Route exact path={"/shop"} render={props => (
              <Shop
                {...props}
                loggedInStatus={this.state.loggedInStatus}
                handleCart={this.handleCart}
              />
            )}
            />
            <Route exact path={"/basket"} render={props => (
              <Basket
                {...props}
                loggedInStatus={this.state.loggedInStatus}
                user={this.state.user}
                handelCart={this.state.handleCart}
              />
            )}
            />
            <Route exact path={"/profile"} render={props => (
              <Profile
                {...props}
                loggedInStatus={this.state.loggedInStatus}
                isAdmin={this.state.isAdmin}
                handleLogin={this.handleLogin}
                handleLogout={this.handleLogout}
              // changeLoginStatus={this.changeLoginStatus}
              />
            )}
            />

            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route path="/login" exact component={Login} />
            <Route path="/signup" exact component={Signup} />
          </Switch>

          {/* Footer */}
          <div className="footer">
            <div className="footer-company">
              <p>
                <a href="#" className="footer-logo">
                  <img src={invertlogo} width="200px" alt="logo" />
                </a>
              </p>
              <p>
                <img src={TelephoneLogo} width="25px" alt="Phone" />
                0469375029
                </p>
              <p>
                <a className="mailto" href="mailto:info@nadaasi.com">
                  <img src={EnvvelopeLogo} width="25px" alt="Mail" />
                  info@nadaasi.com
                </a>
              </p>
            </div>
            <div className="footer-info">
              <h2>Information</h2>
              <br />
              <p><a href="#">Secure Payment</a></p>
              <p><a href="#">Size Chart</a></p>
              <p><a href="#">Privacy Policy</a></p>
              <p><a href="#">Refund Polocy</a></p>
            </div>
            <div className="footer-personal">
              <h2>Your Account</h2>
              <br />
              <p><Link to={'/profile'} className="card-link">Personal info</Link></p>
              <p><Link to={'/profile'} className="card-link">Merchandise Returns</Link></p>
              <p><Link to={'/profile'} className="card-link">Orders</Link></p>
              <p><Link to={'/profile'} className="card-link">Credit slips</Link></p>
              <p><Link to={'/profile'} className="card-link">Addresses</Link></p>
            </div>

          </div>
        </Router >
      </div>
    );
  }
}

export default App;
